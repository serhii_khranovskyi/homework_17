package com.epam.demo;

import com.epam.rd.khranovskyi.mongo.DemoApplication;
import com.epam.rd.khranovskyi.mongo.entity.Account;
import com.epam.rd.khranovskyi.mongo.entity.Address;
import com.epam.rd.khranovskyi.mongo.entity.Customer;
import com.epam.rd.khranovskyi.mongo.service.CustomerService;
import net.bytebuddy.utility.RandomString;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration(classes = DemoApplication.class)
@DataMongoTest
@ComponentScan(basePackages = "com.epam.rd.khranovskyi.mongo")
@ExtendWith(SpringExtension.class)
@DirtiesContext
class DemoApplicationTests {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private MongoTemplate mongoTemplate;

    @BeforeEach
    public void init() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void createCustomer_SaveCustomer() {
        //given
        Customer customer = createCustomer();

        //when
        customerService.save(customer);

        //then
        assertThat(customerService.getAll()).extracting("firstName").contains(customer.getFirstName());
    }

    @Test
    public void givenCreatedCustomer_WhenUpdatedCustomer_ThenAssertThem() {
        //given
        Customer customer = createCustomer();
        customer.setId("testId");
        customerService.save(customer);

        //when
        Customer newCustomer = customerService.getById("testId");
        newCustomer.setFirstName("newName");
        newCustomer.setLastName("newSurname");
        customerService.update("testId",
                newCustomer.getFirstName(),
                newCustomer.getLastName(),
                newCustomer.getAddress(),
                newCustomer.getAccount());

        //then
        assertThat(customerService.getAll().get(0).getFirstName()).isNotSameAs(customer.getFirstName());
    }

    @Test
    public void givenTwoCustomers_WhenGetThemAll_ThenAssertThem() {
        //given
        Customer customer = createCustomer();
        customer.setId("testId");
        customerService.save(customer);

        Customer customer1 = createCustomer();
        customer1.setId("testId2");
        customerService.save(customer1);

        //when
        List<Customer> list = customerService.getAll();

        //then
        assertThat(list
                .stream()
                .map(Customer::getId)
                .collect(Collectors.toList()))
                .isEqualTo(Arrays.asList(customer.getId(),customer1.getId()));
    }

    @Test
    public void givenCustomer_WhenGotById_ThenAssertThem() {
        //given
        Customer customer = createCustomer();
        customer.setId("testId");
        customerService.save(customer);

        //when
        Customer customerGotById = customerService.getById("testId");

        //then
        assertThat(customerGotById).isNotNull();
    }

    @Test
    public void givenCustomer_WhenGotByName_ThenAssertThem(){
        Customer customer = new Customer();
        customer.setId("testId");
        customer.setFirstName("Steve");
        customer.setLastName("Black");
        customer.setAccount(createAccount());
        customer.setAddress(createAddress());
        customerService.save(customer);

        Customer customer1 = createCustomer();
        customer1.setId("testId2");
        customer1.setFirstName("Steve");
        customer1.setLastName("Black");
        customer1.setAccount(createAccount());
        customer1.setAddress(createAddress());
        customerService.save(customer1);

        //when
        List<Customer> list = customerService.getByName("Steve","Black");

        //then
        assertThat(list
                .stream()
                .map(Customer::getId)
                .collect(Collectors.toList()))
                .isEqualTo(Arrays.asList(customer.getId(),customer1.getId()));
    }

    @Test
    public void givenCustomer_WhenGotByAddress_ThenAssertThem(){

        Address address = new Address();
        address.setLine1("line1");
        address.setLine2("line2");
        address.setCountryCode(1);

        Customer customer = new Customer();
        customer.setId("testId");
        customer.setFirstName("Steve");
        customer.setLastName("Black");
        customer.setAccount(createAccount());
        customer.setAddress(address);

        customerService.save(customer);

        Customer customer1 = createCustomer();
        customer1.setId("testId2");
        customer1.setFirstName("John");
        customer1.setLastName("Black");
        customer1.setAccount(createAccount());
        customer1.setAddress(address);
        customerService.save(customer1);

        //when
        List<Customer> list = customerService.getByAddress(address);

        //then
        assertThat(list
                .stream()
                .map(Customer::getId)
                .collect(Collectors.toList()))
                .isEqualTo(Arrays.asList(customer.getId(),customer1.getId()));
    }

    @Test
    public void givenCustomer_WhenGotByCardNumber_ThenAssertThem(){
        Customer customer = createCustomer();
        customerService.save(customer);

        Customer customer1 = customerService.getByCardNumber(customer.getAccount().getCardNumber());

        assertThat(customer.getId()).isEqualTo(customer1.getId());
    }

    @Test
    public void givenThreeCustomers_WhenGotThemByExpirationDate_ThenAssertThem(){
        Customer customer1 = createCustomer();
        Customer customer2 = createCustomer();
        customer2.getAccount().setExpirationDate(LocalDate.of(1999,1,1));
        Customer customer3 = createCustomer();
        customer3.getAccount().setExpirationDate(LocalDate.of(1998,1,1));

        customerService.save(customer1);
        customerService.save(customer2);
        customerService.save(customer3);

        List<Customer> list = customerService.getByExpirationDate();

        assertThat(list
                .stream()
                .map(Customer::getId)
                .collect(Collectors.toList()))
                .isEqualTo(Arrays.asList(customer2.getId(),customer3.getId()));
    }

    private Customer createCustomer() {
        Address address = createAddress();
        Account account = createAccount();
        Customer customer = new Customer();
        customer.setFirstName(RandomString.make(8));
        customer.setLastName(RandomString.make(8));
        customer.setAddress(address);
        customer.setAccount(account);
        return customer;
    }

    private Address createAddress() {
        Address address = new Address();
        address.setLine1(RandomString.make(10));
        address.setLine2(RandomString.make(12));
        address.setCountryCode((int) (1 + Math.random() * 550));
        return address;
    }

    private Account createAccount() {
        Account account = new Account();
        account.setAccountName(RandomString.make(10));
        account.setExpirationDate(LocalDate.of((int) (2010 + Math.random() * 2100), (int) (1 + Math.random() * 12), (int) (1 + Math.random() * 28)));
        account.setCardNumber((int) (1 + Math.random() * 9999));
        return account;
    }
}
