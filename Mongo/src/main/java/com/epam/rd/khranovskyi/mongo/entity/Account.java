package com.epam.rd.khranovskyi.mongo.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document
public class Account {
    @Id
    private Integer cardNumber;
    private String accountName;
    private LocalDate expirationDate;
}
