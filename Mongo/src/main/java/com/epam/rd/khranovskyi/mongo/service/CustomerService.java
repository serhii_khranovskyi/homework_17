package com.epam.rd.khranovskyi.mongo.service;

import com.epam.rd.khranovskyi.mongo.entity.Account;
import com.epam.rd.khranovskyi.mongo.entity.Address;
import com.epam.rd.khranovskyi.mongo.entity.Customer;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {
    List<Customer> getByName(String firstName, String lastName);

    Customer getById(String id);

    List<Customer> getByAddress(Address address);

    Customer getByCardNumber(Integer cardNumber);

    List<Customer> getAll();

    void save(Customer customer);

    Customer update(String id, String first, String last, Address address, Account account);

    List<Customer> getByExpirationDate();
}
