package com.epam.rd.khranovskyi.mongo.service.impl;

import com.epam.rd.khranovskyi.mongo.entity.Account;
import com.epam.rd.khranovskyi.mongo.entity.Address;
import com.epam.rd.khranovskyi.mongo.entity.Customer;
import com.epam.rd.khranovskyi.mongo.repository.CustomerRepository;
import com.epam.rd.khranovskyi.mongo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void save(Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public List<Customer> getAll() {
        return customerRepository.findAll();
    }

    @Override
    public Customer getByCardNumber(Integer cardNumber) {
      return customerRepository.findCustomersByAccount_CardNumber(cardNumber);
    }

    @Override
    public List<Customer> getByExpirationDate(){
        return customerRepository.findCustomersByAccount_ExpirationDateIsLessThan(LocalDate.now());
    }

    @Override
    public List<Customer> getByAddress(Address address) {
        Criteria criteria = Criteria.where("address").is(address);
        Query query = new Query();
        return mongoTemplate.find(query.addCriteria(new Criteria().andOperator(criteria)), Customer.class);
    }

    @Override
    public Customer getById(String id) {
        return customerRepository.findById(id)
                .orElse(null);
    }

    @Override
    public List<Customer> getByName(String firstName, String lastName) {
        System.out.println("First " + firstName + " last " + lastName);
        return customerRepository.findCustomerByFirstNameAndLastName(firstName, lastName);
    }


    @Override
    public Customer update(String id, String first, String last, Address address, Account account) {
        Customer customer = mongoTemplate.findOne(
                Query.query(Criteria.where("id").is(id)), Customer.class);
        assert customer != null;
        customer.setFirstName(first);
        customer.setLastName(last);
        customer.setAddress(address);
        customer.setAccount(account);
        mongoTemplate.save(customer, "customers");
        return customer;
    }
}
