package com.epam.rd.khranovskyi.mongo.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Address {
    private String line1;
    private String line2;
    private Integer countryCode;
}
