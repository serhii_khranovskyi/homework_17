package com.epam.rd.khranovskyi.mongo.controller;

import com.epam.rd.khranovskyi.mongo.entity.Account;
import com.epam.rd.khranovskyi.mongo.entity.Address;
import com.epam.rd.khranovskyi.mongo.entity.Customer;
import com.epam.rd.khranovskyi.mongo.service.CustomerService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping("customers")
public class CustomerController {
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.MM.yyyy");
    @Autowired
    private CustomerService customerService;

    @PostMapping("/create")
    public Customer createCustomer() {
        Address address = new Address(RandomStringUtils.randomAlphabetic(8), RandomStringUtils.randomAlphabetic(8), RandomUtils.nextInt(10, 100));
        Account account = new Account(RandomUtils.nextInt(10, 100), RandomStringUtils.randomAlphabetic(8), java.time.LocalDate.of(RandomUtils.nextInt(1900, 2200), RandomUtils.nextInt(1, 12), RandomUtils.nextInt(1, 28)));

        Customer customer = new Customer(
                RandomStringUtils.randomAlphabetic(8), RandomStringUtils.randomAlphabetic(8), RandomStringUtils.randomAlphabetic(8), address, account
        );
        customerService.save(customer);
        return customer;
    }

    @PostMapping("/update")
    public Customer updateCustomer(@RequestParam String id,
                                   @RequestParam String first,
                                   @RequestParam String last,
                                   @RequestParam String line1,
                                   @RequestParam String line2,
                                   @RequestParam Integer countryCode,
                                   @RequestParam Integer cardNumber,
                                   @RequestParam String accountName,
                                   @RequestParam String date) {
        Address address = new Address(line1, line2, countryCode);
        Account account = new Account(cardNumber, accountName, LocalDate.parse(date, formatter));
        return customerService.update(id, first, last, address, account);
    }

    @GetMapping(path = "/{id}")
    public Customer getCustomerById(@PathVariable String id) {
        return customerService.getById(id);
    }

    @GetMapping
    public List<Customer> getAll() {
        return customerService.getAll();
    }

    @GetMapping(path = "/searchByName")
    public List<Customer> getClientByName(@RequestParam String first, @RequestParam String last) {
        return customerService.getByName(first, last);
    }

    @GetMapping(path = "/searchByAddress")
    public List<Customer> getClientByAddress(@RequestParam String line1,
                                             @RequestParam String line2,
                                             @RequestParam Integer countryCode) {
        Address address = new Address(line1, line2, countryCode);
        return customerService.getByAddress(address);
    }

    @GetMapping(path = "/date")
    public List<Customer> getExpirationDate() {
        return customerService.getByExpirationDate();
    }

    @GetMapping(path = "/searchByCardNumber")
    public Customer getClientByCardNumber(@RequestParam Integer card) {
        return customerService.getByCardNumber(card);
    }

}
