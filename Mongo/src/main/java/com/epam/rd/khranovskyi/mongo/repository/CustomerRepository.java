package com.epam.rd.khranovskyi.mongo.repository;

import com.epam.rd.khranovskyi.mongo.entity.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CustomerRepository extends MongoRepository<Customer, String> {
    List<Customer> findCustomerByFirstNameAndLastName(String firstName, String lastName);

    Customer findCustomersByAccount_CardNumber(Integer cardNumber);

 // @Query("{'expirationDate' : { $lte : ?0 }}")
    List<Customer> findCustomersByAccount_ExpirationDateIsLessThan(LocalDate date);
}
